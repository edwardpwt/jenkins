# Jenkins Master

Jenkins CI Server Image with Git, Bitbucket plugin and Docker.

## Getting Started

These instructions will get you a Dockerfile for building Jenkins Image.

### Prerequisites

Docker installed on the hosting machine

### Installing

A quick start:

```
./ container.sh
```

The above shell script will build a jenkins docker image named jenkins:bitbucket will the necessary plugins and run a container based the image created called jenkinsci on port 3003.

## Configuring Webhook

In short, navigate to setting and webhooks.

Click on "Add Webhook"

Give tile and URL http://<host>:<3003>/bitbucket-hook/ with the Jenkins's host and port.

Select Trigger: Repository push.

Done

* [Manage webhooks](https://confluence.atlassian.com/bitbucket/manage-webhooks-735643732.html) - Details on Webhooks Configure.


### Jenkins Configure

Visit http://<host>:<3003> of the Jenkins host.

Click "create new project"

Click "Freestyle Project"

Click "OK"

At "Source Code Management"

Choose Git, fill in the repository URL and credentials (if needed).

At "Build Triggers"

Choose "Build when a change is pushed to BitBucket"

At "Build"

Write the shell commands needed to build the container.

Click "Save"

### Build

The Build will be triggered whenever change is pushed or manually triggered.

## License

TODO