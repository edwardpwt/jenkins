#!/bin/bash
docker build -t jenkins:bitbucket .
docker run -d -p 3003:8080 -v /var/run/docker.sock:/var/run/docker.sock -v /usr/bin/docker:/usr/bin/docker --name jenkinsci --restart=on-failure[:3] jenkins:bitbucket